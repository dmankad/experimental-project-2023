# ML for Event Selection 

---

## Table of Contents
- [Introduction](#introduction)
- [Workflow](#workflow)
- [Repository Architecture](#repo-architecture)

## Introduction <a name="introduction"></a>

This repository contains the machinery necessary to design, train, and analyze the performance of ML models to distinguish between signal events and background events.

The `ntuple`s (provided as `.root` files) are the primary external inputs to the tasks accomplished using this repository.
Each `ntuple` contains various simulated events of a particular high-energy process of interest.

## Workflow 

Given these `ntuple`s, the workflow for the tasks to be accomplished using this repository is as follows:

1. **Preliminary Processing of `ntuple`s**
    - We first simplify the `ntuple`s and convert them to a much more efficiently readable format called `feather` files.
    - There are various reasons for wanting to do this:
        - The `ntuple`s often contain more information than we need for the ML purposes.
        - There might as well be some derived information that we want to extract via processing the raw information available in `ntuple`s.
            - Of course, such information can be added to the `ntuple`s themselves, however, it's computationally more expensive to reproduce `ntuple`s than to reprocess the produced `ntuple`s. 
        - The structure of the data in `ntuple`s is often "non-linear", i.e., it cannot be represented as a matrix but would rather need to be represented as [jagged arrays](https://en.wikipedia.org/wiki/Jagged_array).
        - They are not ideal to work with in `python`, especially as they grow larger.
    - This is accomplished using the `./src/process_ntuples.py` script.


## Repository Architecture 

```
.
├── README.md
├── requirements.txt
├── run
│   ├── configs
│   │   ├── config_v1.yaml
│   │   ├── ntuples_default.yaml
│   │   └── ntuples_v1.yaml
│   └── run.py
├── setup.sh
└── src
    ├── __init__.py
    ├── data_preparation
    │   ├── __init__.py
    │   └── process_ntuples.py
    └── utils
        ├── __init__.py
        ├── configure.py
        ├── constants.py
        ├── ioutils.py
        └── logger.py
```
