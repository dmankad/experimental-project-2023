import os
import glob
import yaml
import json
from collections import OrderedDict

def load_yaml(path):
    with open(path, "r") as f:
        return OrderedDict( yaml.load(f, Loader=yaml.FullLoader) )


def load_json(path):
    with open(path, "r") as f:
        return OrderedDict( json.load(f) )


def dump_yaml(d, path):
    with open(path, "w") as f:
        yaml.dump(d, f, indent=4)


def dump_json(d, path):
    with open(path, "w") as f:
        json.dump(d, f, indent=4)


def load_dict(path):
    ext = os.path.basename(path).split(".")[-1]
    if ext == "yaml" or ext == "yml":
        return load_yaml(path)
    elif ext == "json":
        return load_json(path)
    else:
        raise ValueError("Unknown file extension: " + ext)


yaml_load = load_yaml
yaml_dump = dump_yaml
json_load = load_json
json_dump = dump_json
dict_load = load_dict

def pickle_save(data, filename, text = ""):
    dir_check(filename)
    with open(filename, "wb") as f:
        pickle.dump(data, f)
    if text:
        print(yellow(text), yellow(filename))

def pickle_load(filename):
    with open(filename, "rb") as f:
        return pickle.load(f)


def get_path(filename, dirname=os.getcwd(), mode = "input"):
    if os.path.exists(filename):
        if mode == "output":
            logger.warning(f"File already exists: {filename}!")
        return filename
    else:
        if os.path.isabs(filename):
            if mode == "input":
                raise FileNotFoundError(f"File not found: {filename}")
            if mode == "output":
                base_dir = os.path.dirname(filename)
                if os.path.exists(base_dir):
                    return filename
                else:
                    raise FileNotFoundError(f"Directory not found: {base_dir}")
        else:
            return get_path(os.path.join(dirname, filename), mode=mode)


def make_list(pattern):
    return glob.glob(pattern)


def get_files(input_data, input_dir=""):
    if input_data == None:
        return []

    if not isinstance(input_data, list):
        input_data = [input_data]

    input_data_list = []

    for i, item in enumerate(input_data):
        if (input_dir != "") and (not item.startswith("/")):
            input_data[i] = os.path.join(input_dir, item)
            item = input_data[i]
        input_data_list += make_list(item)

    return input_data_list
