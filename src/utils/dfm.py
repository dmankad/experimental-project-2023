#!/usr/bin/env python3

import math
import datetime
import pickle
import pandas as pd
import numpy as np
import json
import yaml
import glob
import sys
import re
import psutil
import itertools
import os
from os import path
import matplotlib.pyplot as plt
from fnmatch import fnmatch

from src.utils import ioutils
from src.utils import logger

logger = logger.get_logger(__name__)
#from ml_share import ml_config

import warnings
warnings.simplefilter(action = "ignore", category = FutureWarning)

import uproot3 as uproot
import uproot3_methods as um

##################


##################

def get_selection(selection, config):  
    def get_sel(selection):
        if selection:
            return selection
        else:
            logger.warning("Selection(s) not set! Using TRUE instead. Check the final selection string!")
            return "( True )"

    separators = [
        " ",
        "|",
        "&",
        "(",
        ")",
    ]
    default_separator = separators[0]

    to_split = selection

    for separator in separators[1:]:
        to_split = to_split.replace(separator, default_separator)
    to_split = [i.strip() for i in to_split.split(default_separator)]
    to_split = [i for i in to_split if fnmatch(i, "$*")]

    for item in to_split:
        selection = selection.replace(item, get_sel(config.get(item.replace("$", ""))))

    if "$" in selection:
        selection = get_selection(selection, config)

    return selection

def get_features_from_string(input_string = ""):
    """
    description: Get the feature names from a string, e.g. df["X"]] -> X.
    arguments:
    - input_string: string to be parsed
    returns: list of feature names
    """
    return re.findall(r"df\[\"([^\s\[\"\]]+)\"\]", input_string)

def get_filetype(input_data):  # Return the file type from the input filename
    filetype = path.splitext(input_data)[1]

    if filetype == ".csv":
        return "csv"
    elif filetype in [ ".hdf", ".h5", ".hdf5" ]:
        return "hdf"
    elif filetype in [ ".feather", ".f", ".fth", ".ftr" ]:
        return "feather"
    else:
        logger.critical(sys._getframe().f_code.co_name + ": unknown filetype " + input_data + ".")
        raise SystemError
def df_save(dfs, filename, text = "Saved to:", save_index = False):  # Save the pandas dataframe "dfs" to the file "filename"
    time_start = datetime.datetime.now()
    dir_check(filename)

    if isinstance(dfs, list):
        df = pd.concat(dfs, ignore_index = True)
    else:
        df = dfs

    df.replace([np.inf, -np.inf], np.nan, inplace = True)
    df.dropna(inplace = True)

    filetype = get_filetype(filename)

    if filetype == "csv":
        df.to_csv(filename, index = save_index)
    elif filetype == "hdf":
        df.to_hdf(filename, mode = "w", key = "data", index = save_index)
    elif filetype == "feather":
        if path.exists(filename):
            os.remove(filename)
        df.reset_index(drop = ( not save_index )).to_feather(filename)

    if text:
        logger.info(text + " " + filename + " in " + str(datetime.datetime.now() - time_start))

    return df

def add_features( df = None, features_to_add = [] ):  # Add the features in "features_to_add" to the dataframe "df"
    time_start = datetime.datetime.now()
    return df

def df_load(input_data, items_to_load = [], selection = "", probabilities = "", proba_names = "", probability_cut = "", threads = None, memory = None, chunksize = None):   # Further process the dataframe "input_data" (e.g. add additional features, apply additional selection) and return it
    time_start = datetime.datetime.now()
    
    ######## If "input_data" is a dataframe ########
    if isinstance(input_data, pd.DataFrame):
        if probabilities:
            logger.critical("Provided probability files will not be used!")

        # TODO: still needed?
        df = input_data.replace([np.inf, -np.inf], np.nan).dropna()

        items_in_df       = df.columns.values.tolist()
        if len(items_to_load) == 0:
            items_to_load = items_in_df
        selection_list    = get_features_from_string(selection) + get_features_from_string(probability_cut)
        features_to_add   = list(set(items_to_load + selection_list) - set(items_in_df))
        add_features_list = add_features(features_to_add = features_to_add)
        items_needed      = list(set(items_to_load + selection_list + add_features_list))

        add_features(df, features_to_add)   # Add the missing features in "itmes_to_load" to "df" 

        if selection and ( selection != "True" or selection != "( True )" ):  # Apply additional selection to the dataframe
            print(blue("Applying additional selection:"), red(selection) if "( True )" in selection else selection)
            df = df[eval(selection, { "__builtins__" : {}, "df" : df } )].copy()

        if probability_cut and ( probability_cut != "True" or probability_cut != "( True )" ):  # Apply probability cut to the dataframe
            print(blue("Applying additional probability_cut:"), red(probability_cut) if "( True )" in probability_cut else probability_cut)
            df = df[eval(probability_cut, { "__builtins__" : {}, "df" : df } )].copy()

        if len(items_to_load) > 0:    # Keep only the columns in "items_to_load"
            items_to_load = list(dict.fromkeys(items_to_load))
            df = df.reindex(columns = items_to_load)
    
    ######## If "input_data" is a list of feather filenames ########
    else:
        if not isinstance(input_data, list):
            input_data = ioutils.make_list(input_data)

        if len(input_data) == 0:
            logger.critical(sys._getframe().f_code.co_name + ": no input data? Returning `False`!")
            return False

        if probabilities:
            if not isinstance(probabilities, list):
                probabilities = [ probabilities ]

            if proba_names:
                if not isinstance(proba_names, list):
                    proba_names = [ proba_names ]

                if len(probabilities) != len(proba_names):
                    logger.critical(sys._getframe().f_code.co_name + ": prob. list != name list!")
                    raise SystemError

                if len(set(proba_names)) != len(proba_names):
                    logger.critical(sys._getframe().f_code.co_name + ": names are not unique!")
                    raise SystemError
            else:
                proba_names = [ None ] * len(probabilities)

        if probabilities:
            if isinstance(probabilities, list):
                logger.info("Reading probabilities:")
                print_nice(probabilities)
            else:
                print(blue("Reading probabilities:"), probabilities)

        if selection and ( selection != "True" or selection != "( True )" ):
            print(blue("Applying additional selection:"), red(selection) if "( True )" in selection else selection)

        if probability_cut and ( probability_cut != "True" or probability_cut != "( True )" ):
            print(blue("Applying additional probability_cut:"), red(probability_cut) if "( True )" in probability_cut else probability_cut)

        filetype = get_filetype(input_data[0])

        if filetype == "hdf" or filetype == "feather":
            dfs                           = []
            items_needed                  = []
            selection_list                = []
            features_to_add               = []
            add_features_before_selection = False
            first_file                    = True

            for file in input_data:
                print("Reading:", file)

                if filetype == "feather":
                    df = pd.read_feather(file)

                df.reset_index(drop = True, inplace = True)

                if first_file:
                    items_in_df       = df.columns.values.tolist()
                    if len(items_to_load) == 0:
                        items_to_load = items_in_df
                    selection_list    = get_features_from_string(selection) + get_features_from_string(probability_cut)
                    features_to_add   = list(set(items_to_load + selection_list) - set(items_in_df))
                    add_features_list = add_features(features_to_add = features_to_add)
                    items_needed      = list(set(items_to_load + selection_list + add_features_list))

                    items_needed = list(set(items_in_df) & set(items_needed))
                    add_features_before_selection = not set(selection_list).issubset(set(items_in_df))

                    first_file = False

                if len(items_to_load) > 0 and set(items_to_load) == set(df.columns.values.tolist()):
                    items_to_load = []

                if len(items_needed) > 0:
                    df = df.reindex(columns = items_needed)

                if len(probabilities) > 0:
                    for proba,proba_name in zip(probabilities,proba_names):
                        name = path.splitext(path.basename(file))[0]
                        proba_file = proba.replace("*", name)
                        proba_filetype = get_filetype(proba_file)

                        if proba_filetype == "feather":
                            proba_df = pd.read_feather(proba_file)
                        elif proba_filetype == "hdf":
                            proba_df = pd.read_hdf(proba_file)
                        elif proba_filetype == "csv":
                            proba_df = pd.read_csv(proba_file)
                        else:
                            logger.critical(sys._getframe().f_code.co_name + ": unknown filetype " + file[0] + ".")
                            raise SystemError

                        if proba_name:
                            rename_dict = {}
                            for name in proba_df.columns.values.tolist():
                                # TODO:
                                rename_dict[name] = name.replace("proba_gnn1", proba_name)
                                if rename_dict[name] == name:
                                    rename_dict[name] = name.replace("proba_gnn2", proba_name)
                            # proba_df.columns = [ proba_name ]
                            proba_df.rename(columns = rename_dict, inplace = True)

                        proba_df.reset_index(drop = True, inplace = True)

                        if len(df) != len(proba_df):
                            if selection and ( selection != "True" or selection != "( True )" ) and len(df[eval(selection, { "__builtins__" : {}, "df" : df } )]) == len(proba_df):
                                df = df.loc[eval(selection, { "__builtins__" : {}, "df" : df } ), :]
                                df.reset_index(drop = True, inplace = True)
                            else:
                                logger.critical(sys._getframe().f_code.co_name + ": different lengths!")
                                logger.debug(file, proba_file)
                                logger.debug(df.shape)
                                logger.debug(proba_df.shape)
                                raise SystemError

                        df_new = df.join(proba_df)

                        if df_new.isnull().values.any() or len(df_new) != len(proba_df):
                            logger.critical(sys._getframe().f_code.co_name + ": join() error!")
                            print(file, proba_file)
                            print(df_new.shape)
                            print(proba_df.shape)
                            quit()

                        df = df_new

                if add_features_before_selection:
                    add_features(df, features_to_add)

                if selection and ( selection != "True" or selection != "( True )" ):
                    df = df[eval(selection, { "__builtins__" : {}, "df" : df } )].copy()

                if probability_cut and ( probability_cut != "True" or probability_cut != "( True )" ):
                    df = df[eval(probability_cut, { "__builtins__" : {}, "df" : df } )].copy()

                if not add_features_before_selection:
                    add_features(df, features_to_add)

                if len(items_to_load) > 0:
                    items_to_load = list(dict.fromkeys(items_to_load))
                    for item in items_to_load:
                        if item not in df.columns.values.tolist():
                            logger.warn("Item \"" + item + "\" not in " + file + "!")

                    df = df.reindex(columns = items_to_load)

                dfs.append(df)

            df = pd.concat(dfs, ignore_index = True)

        logger.info(f"Loaded: {df.shape} in {(datetime.datetime.now() - time_start)}")

    return df

def df_view(df, selection = ""):
    if selection:
        mask = eval(selection, { "__builtins__" : {}, "df" : df } )
        print(blue("Applying additional selection:"), red(selection) if "( True )" in selection else selection, "--> get", len(df[mask]), "candidates.")
        return df.loc[mask]
    else:
        return df

def get_n_best(df, feature, n = 1, m = 1, sort = "highest"):
    if n == 0:
        return df

    if m > n:
        logger.fatal(sys._getframe().f_code.co_name + ": m > n!")
        raise SystemExit
    if m < 1:
        logger.fatal(sys._getframe().f_code.co_name + ": m < 1!")
        raise SystemExit

    if n < 0:
        logger.fatal(sys._getframe().f_code.co_name + ": n < 0!")
        raise SystemExit

    if m > 1:
        logger.info("Applying n_best_" + feature + " in (" + str(m) + ", " + str(n) + ")")
    else:
        logger.info("Applying n_best_" + feature + " = " + str(n))

    df_apply = df

    # Using selected_indices is not really needed, only because of the backward compatibility (sorting)
    selected_indices = df_apply.sort_values(
            by = [feature], ascending = True if sort == "lowest" else False
            ).groupby( "event_number", as_index = False, sort = False ).nth( list( range( m-1, n ) ) ).index
    df_best = df_apply[df_apply.index.isin(selected_indices)]

    return df_best
