import torch

from src.utils import logger
logger = logger.get_logger(__name__)
if torch.cuda.is_available():
    ml_device = torch.device("cuda:0")
    logger.info("Running ML on the GPU...")
else:
    ml_device = torch.device("cpu")
    logger.info("Running ML on the CPU...")
