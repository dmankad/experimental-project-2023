import particle
from particle import Particle
import hepunits

masses = {}
masses["muon"] = Particle.from_name("mu+").mass
