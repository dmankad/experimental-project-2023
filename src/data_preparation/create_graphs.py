import sys

import torch

import torch.multiprocessing as mp

import json
import dgl
import datetime
import pandas as pd
import numpy as np
from os import path
from functools import partial
from collections import OrderedDict

from src.utils import system
from src.utils import dfm
from src.utils import logger

logger = logger.get_logger( __name__ )
#from ml_share import utils

def collate_graphs(samples):
    graphs = [ x[0] for x in samples ]
    labels = [ x[1] for x in samples ]
    batched_graph = dgl.batch(graphs)

    goblins = [ x[ 0 ].gdata["features"] for x in samples ]
    batched_graph.gdata = {}
    batched_graph.gdata["features"] = torch.stack( tuple( goblins ), dim = 0 )

    # targets = torch.tensor(labels, device = system.ml_device).long()
    targets = torch.stack(labels).to(system.ml_device).squeeze(-1).long()

    return batched_graph, targets

def df_row_to_tensors(row, tensor_list):
    tensor_list_inflated = False
    if not isinstance( tensor_list[ 0 ], list ):
        tensor_list = [ tensor_list ]
        tensor_list_inflated = True

    row_tensors = []
    for tensor in tensor_list:
        row_tensor_features = []
        for feature in tensor:
            if feature == "empty_feature":
                row_tensor_features.append( float( 0 ) )
            else:
                row_tensor_features.append( float( row[ feature ] ) )
        row_tensors.append(row_tensor_features)

    if tensor_list_inflated:
        row_tensors = row_tensors[ 0 ]

    return row_tensors

def fill_graph( row ):
    g = dgl.graph(
            (
                torch.tensor( json.loads( row.src_nodes ), device = system.ml_device ), 
                torch.tensor( json.loads( row.dst_nodes ), device = system.ml_device )
            )
        )

    g.gdata = {}
    g.gdata["features"] = torch.tensor( row.goblins, device = system.ml_device )
    g.ndata["features"] = torch.tensor( row.nodes  , device = system.ml_device )
    g.edata["features"] = torch.tensor( row.edges  , device = system.ml_device )

    return g

def df_add_graph_columns(df, graph_dict, normalization = None, features = None):
    df[ "src_nodes" ] = None
    df[ "dst_nodes" ] = None
    df[ "nodes"     ] = None
    df[ "edges"     ] = None
    df[ "goblins"   ] = None
    df[ "_row_used" ] = False

    for item in graph_dict.values():
        selection = eval( ( item["selection"] + " & " if item["selection"] else "" ) + '(~df["_row_used"])', { "__builtins__" : {}, "df" : df } )

        if isinstance(normalization, pd.DataFrame) and "_norm" in normalization.columns.values.tolist():
            df.loc[selection, features] = ( df.loc[selection, features] - normalization.set_index("_norm").loc["mean", features] ) / normalization.set_index("_norm").loc["std", features]

        df.loc[selection, "src_nodes"] = str(item["src_nodes"])
        df.loc[selection, "dst_nodes"] = str(item["dst_nodes"])
        df.loc[selection, "nodes"]     = df.loc[selection, :].apply(lambda row: df_row_to_tensors(row, item["node_list"]),   axis = 1)
        df.loc[selection, "edges"]     = df.loc[selection, :].apply(lambda row: df_row_to_tensors(row, item["edge_list"]),   axis = 1)
        df.loc[selection, "goblins"]   = df.loc[selection, :].apply(lambda row: df_row_to_tensors(row, item["goblin_list"]), axis = 1)
        df.loc[selection, "_row_used"] = True

    return df

def preparator(input_data, parameters, return_nums = True):
    logger.info("Preparing graphs...")

    time_start = datetime.datetime.now()

    with open(parameters["graph_dict"], "r" ) as f:
        graph_features_dict = json.load( f, object_pairs_hook = OrderedDict )
        """
        comment-block:
            tags:
            - code
            - explanation
            id: 18032023T030811+0200DCM
            comments:
            - The reason behind loading the `JSON` file as an 
            `OrderedDict` is to ensure that when we fill out features
            into the feature-vector at a node, especially in homogeneous
            graphs, the order in which the entries go into a given
            feature-vector is not changing from candidate to candidate.
            - If we do not fix the order of keys in the dictionary, it
            might happen that in one candidate, the feature-vector on
            the electron1-node is of the form [E, p, eta, phi] but in
            the second candidate, the same feature-vector on the same
            node is of the form [p, eta, E, phi].
            - So, loading the `JSON` as an `OrderedDict` is VERY
            important in this case.
        """

    if not isinstance(parameters["graph_version"], dict):
        parameters["graph_version"] = { "default": { "selection": "", "graph_topo": parameters["graph_version"] } }

    num_node_features   = 1
    num_edge_features   = 1
    num_goblin_features = 1

    super_graph_dict = {}
    for key, value in parameters["graph_version"].items():
        super_graph_dict[key] = {
            "selection" : dfm.get_selection(value["selection"], parameters["full_config_dict"]["common"]),
            "graph_topology" : graph_features_dict[ value["graph_topo"] ]
        }

        # patch for bad configs
        graph_features_dict[value["graph_topo"]]["goblins"] = [ feature for feature in graph_features_dict[value["graph_topo"]].get("goblins", []) if feature != "empty_feature"]

        if "infect_nodes" in parameters["goblins_plan"]:
            for node in list( graph_features_dict[value["graph_topo"]]["nodes"].keys() ):
                graph_features_dict[value["graph_topo"]]["nodes"][ node ] += graph_features_dict[value["graph_topo"]]["goblins"]

        if "infect_edges" in parameters["goblins_plan"]:
            for edge in list( graph_features_dict[value["graph_topo"]]["edges"].keys() ):
                graph_features_dict[value["graph_topo"]]["edges"][ edge ] += graph_features_dict[value["graph_topo"]]["goblins"]

        num_node_features   = max( [num_node_features] + [ len(fl) for fl in graph_features_dict[value["graph_topo"]]["nodes"].values() ] )
        num_edge_features   = max( [num_edge_features] + [ len(fl) for fl in graph_features_dict[value["graph_topo"]]["edges"].values() ] )
        num_goblin_features = max( num_goblin_features, len( graph_features_dict[value["graph_topo"]]["goblins"] ) )

    for graph_dict in super_graph_dict.values():
        nodes = list( graph_dict["graph_topology"]["nodes"].keys() )
        graph_dict["src_nodes"] = list( nodes.index(item[0]) for item in graph_dict["graph_topology"]["src_dst_nodes"] )
        graph_dict["dst_nodes"] = list( nodes.index(item[1]) for item in graph_dict["graph_topology"]["src_dst_nodes"] )

        g = dgl.graph((torch.tensor(graph_dict["src_nodes"]), torch.tensor(graph_dict["dst_nodes"])))

        graph_dict["num_nodes"] = g.num_nodes()
        graph_dict["num_edges"] = g.num_edges()
        
        graph_dict["node_list"] = [[]] * graph_dict["num_nodes"]
        graph_dict["edge_list"] = [[]] * graph_dict["num_edges"]

        for node, features in graph_dict["graph_topology"]["nodes"].items():
            graph_dict["node_list"][ nodes.index(node) ] = features + ["empty_feature"] * ( num_node_features - len(features) )

        for edge_index in range(graph_dict["num_edges"]):
            src_node_name = nodes[int( g.find_edges( edge_index )[ 0 ] )]
            dst_node_name = nodes[int( g.find_edges( edge_index )[ 1 ] )]
            edge_name     = src_node_name.replace( "-node", "" ) + "-" + dst_node_name.replace( "-node", "" ) + "-edge"
            features      = graph_dict["graph_topology"]["edges"][ edge_name ]

            graph_dict["edge_list"][ edge_index ] = features + ["empty_feature"] * ( num_edge_features - len(features) )

        graph_dict["goblin_list"] = graph_dict["graph_topology"]["goblins"] + ["empty_feature"] * ( num_goblin_features - len( graph_dict["graph_topology"]["goblins"] ) )

    normalization = None
    if len(parameters["norm_base"]) > 0:
        parameters["normalization_path"] = path.join(parameters["output_dir"], "normalization.csv")
        if path.isfile(parameters["normalization_path"]):
            normalization = dfm.df_load(parameters["normalization_path"])
        else:
            logger.critical("Cannot find the normalization file! This should never happen!")
            raise FileNotFoundError

    df_add_graph_columns_partial = partial(df_add_graph_columns, graph_dict = super_graph_dict, normalization = normalization, features = parameters["features"])

    n_cores    = max(1, mp.cpu_count() - 2)
    df_split   = np.array_split(input_data, n_cores)
    pool       = mp.Pool(n_cores)
    df_tmp     = pool.map(df_add_graph_columns_partial, df_split)
    input_data = pd.concat(df_tmp)
    pool.close()
    pool.join()

    graphs = []

    if len(dfm.df_view(input_data, '(~df["_row_used"])')) > 0:
        logger.critical("Not using all input data, something is wrong!")
        raise SystemExit

    for row in input_data.itertuples():
        g = fill_graph(row)
        graphs.append(g)

    samples = graphs

    logger.info(f"\tDone in: {datetime.datetime.now()-time_start}")

    if return_nums:
        num_nodes = max( [ item["num_nodes"] for item in super_graph_dict.values() ] )
        num_edges = max( [ item["num_edges"] for item in super_graph_dict.values() ] )

        return samples, num_node_features, num_edge_features, num_goblin_features, num_nodes, num_edges
    else:
        return samples
