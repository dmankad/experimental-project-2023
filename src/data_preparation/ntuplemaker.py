### Imports ###
### ------- ###

### Imports -> Matrix/Array Manipulation ###
### ------------------------------------ ###
import numpy as np
import pandas as pd
import awkward as ak

### Imports -> I/O Utilities ###
### ------------------------ ###
import os
import uproot

### Imports -> Scientific Utilities ###
### ------------------------------- ###
import vector

### Imports -> Pythonic Utilities ###
### ----------------------------- ###
import collections

### Imports -> Internals ###
### -------------------- ###
from src.utils import constants
from src.utils import configure
from src.utils import logger as logging


class NTupleMaker:
    def __init__(self, config_path, logger=None):
        self.config = configure.load_config(config_path)
        self.logger = logging.get_logger(__name__) if logger is None else logger

    ### Utility Functions ###
    ### ----------------- ###
    @staticmethod
    def df_to_namedtuple(df, name="events"):
        namedClass = collections.namedtuple(name, df.columns)
        values = [ak.from_iter(df[col].tolist()) for col in df.columns]
        namedTuple = namedClass._make(values)
        return namedTuple

    ### Loading the Data ###
    ### ---------------- ###
    def load_ntuple(
        self, ntuple, tree_name=None, branch_selection=None, max_events=None
    ):
        tree_name = (
            self.config.get("tree_name", None) if tree_name is None else tree_name
        )
        branch_selection = (
            self.config.get("branch_selection", None)
            if branch_selection is None
            else branch_selection
        )
        max_events = (
            self.config.get("max_events", None) if max_events is None else max_events
        )
        ttree = uproot.open(ntuple)[tree_name]
        events = self.df_to_namedtuple(
            ttree.arrays(branch_selection, library="pd", entry_stop=max_events)
        )
        return events

    ### Processing the Data ###
    ### ------------------- ###
    def decorate_and_flatten(self, events, physics=None, dsid = None):
        physics = self.config.get("physics", None) if physics is None else physics
        if physics == "Zprime->mumu":
            muon0_pt = events.mu_pt[:, 0]
            muon0_eta = events.mu_eta[:, 0]
            muon0_phi = events.mu_phi[:, 0]
            muon0_charge = events.mu_q[:, 0]

            muon1_pt = events.mu_pt[:, 1]
            muon1_eta = events.mu_eta[:, 1]
            muon1_phi = events.mu_phi[:, 1]
            muon1_charge = events.mu_q[:, 1]

            lead_bjet_pt = events.bjet_pt[:, 0]
            lead_bjet_eta = events.bjet_eta[:, 0]
            lead_bjet_phi = events.bjet_phi[:, 0]
            lead_bjet_energy = events.bjet_e[:, 0]

            lead_jet_pt = events.jet_pt[:, 0]
            lead_jet_eta = events.jet_eta[:, 0]
            lead_jet_phi = events.jet_phi[:, 0]
            lead_jet_energy = events.jet_e[:, 0]

            muon0_p4 = vector.MomentumObject4D.from_rhophietatau(
                muon0_pt, muon0_phi, muon0_eta, constants.masses["muon"]
            )
            muon1_p4 = vector.MomentumObject4D.from_rhophietatau(
                muon1_pt, muon1_phi, muon1_eta, constants.masses["muon"]
            )
            lead_bjet_p4 = vector.MomentumObject4D.from_rhophietat(
                lead_bjet_pt, lead_bjet_phi, lead_bjet_eta, lead_bjet_energy
            )
            lead_jet_p4 = vector.MomentumObject4D.from_rhophietat(
                lead_jet_pt, lead_jet_phi, lead_jet_eta, lead_jet_energy
            )
            Zp_p4 = muon0_p4 + muon1_p4

            df = pd.DataFrame()

            df["Zp_mass"] = ak.flatten(Zp_p4.mass, axis=None).to_numpy()
            df["Zp_pt"] = ak.flatten(Zp_p4.pt, axis=None).to_numpy()
            df["Zp_eta"] = ak.flatten(Zp_p4.eta, axis=None).to_numpy()
            df["Zp_phi"] = ak.flatten(Zp_p4.phi, axis=None).to_numpy()
            df["Zp_energy"] = ak.flatten(Zp_p4.E, axis=None).to_numpy()

            df["muon0_pt"] = ak.flatten(muon0_pt, axis=None).to_numpy()
            df["muon0_eta"] = ak.flatten(muon0_eta, axis=None).to_numpy()
            df["muon0_phi"] = ak.flatten(muon0_phi, axis=None).to_numpy()
            df["muon0_energy"] = ak.flatten(muon0_p4.E, axis=None).to_numpy()
            df["muon0_charge"] = ak.flatten(muon0_charge, axis=None).to_numpy()

            df["muon1_pt"] = ak.flatten(muon1_pt, axis=None).to_numpy()
            df["muon1_eta"] = ak.flatten(muon1_eta, axis=None).to_numpy()
            df["muon1_phi"] = ak.flatten(muon1_phi, axis=None).to_numpy()
            df["muon1_energy"] = ak.flatten(muon1_p4.E, axis=None).to_numpy()
            df["muon1_charge"] = ak.flatten(muon1_charge, axis=None).to_numpy()

            df["lead_bjet_pt"] = ak.flatten(lead_bjet_p4.pt, axis=None).to_numpy()
            df["lead_bjet_eta"] = ak.flatten(lead_bjet_p4.eta, axis=None).to_numpy()
            df["lead_bjet_phi"] = ak.flatten(lead_bjet_p4.phi, axis=None).to_numpy()
            df["lead_bjet_energy"] = ak.flatten(lead_bjet_p4.E, axis=None).to_numpy()

            df["lead_jet_pt"] = ak.flatten(lead_jet_p4.pt, axis=None).to_numpy()
            df["lead_jet_eta"] = ak.flatten(lead_jet_p4.eta, axis=None).to_numpy()
            df["lead_jet_phi"] = ak.flatten(lead_jet_p4.phi, axis=None).to_numpy()
            df["lead_jet_energy"] = ak.flatten(lead_jet_p4.E, axis=None).to_numpy()

            df["deltaR_Zp_bjet"] = ak.flatten(
                lead_bjet_p4.deltaR(Zp_p4), axis=None
            ).to_numpy()
            df["deltaR_Zp_jet"] = ak.flatten(
                lead_jet_p4.deltaR(Zp_p4), axis=None
            ).to_numpy()
            df["deltaR_Zp_muon0"] = ak.flatten(
                muon0_p4.deltaR(Zp_p4), axis=None
            ).to_numpy()
            df["deltaR_Zp_muon1"] = ak.flatten(
                muon1_p4.deltaR(Zp_p4), axis=None
            ).to_numpy()
            df["deltaR_bjet_jet"] = ak.flatten(
                lead_bjet_p4.deltaR(lead_jet_p4), axis=None
            ).to_numpy()
            df["deltaR_muon0_bjet"] = ak.flatten(
                muon0_p4.deltaR(lead_bjet_p4), axis=None
            ).to_numpy()
            df["deltaR_muon0_jet"] = ak.flatten(
                muon0_p4.deltaR(lead_jet_p4), axis=None
            ).to_numpy()
            df["deltaR_muon1_bjet"] = ak.flatten(
                muon1_p4.deltaR(lead_bjet_p4), axis=None
            ).to_numpy()
            df["deltaR_muon1_jet"] = ak.flatten(
                muon1_p4.deltaR(lead_jet_p4), axis=None
            ).to_numpy()
            df["deltaR_muon0_muon1"] = ak.flatten(
                muon0_p4.deltaR(muon1_p4), axis=None
            ).to_numpy()

            df["MET_X"] = ak.flatten(
                events.MET_X[:, 0], axis=None
            ).to_numpy()  # why not always 1 value per event?
            df["MET_Y"] = ak.flatten(
                events.MET_Y[:, 0], axis=None
            ).to_numpy()  # why not always 1 value per event?
            df["MET_sum"] = ak.flatten(
                events.MET_sum[:, 0], axis=None
            ).to_numpy()  # why not always 1 value per event?
            df["MET_quad"] = ak.flatten(
                events.MET_quad[:, 0], axis=None
            ).to_numpy()  # why not always 1 value per event?

        if physics == "Zprime->mumu_v2":
            df = pd.DataFrame()
            for item in events._fields:
                df[item] = eval(f"events.{item}").to_numpy()
        if dsid is not None:
            df["info_sample"] = np.full(len(df), dsid)
        return df
    def run(self, ntuples, dsids = None ):
        for counter, ntuple in enumerate(ntuples):
            self.logger.info(f"Processing [{counter+1}/{len(ntuples)}]: {ntuple}...")
            for _, tree_info in self.config["trees"].items():
                events = self.load_ntuple(
                    ntuple,
                    tree_name=tree_info["tree_name"],
                    branch_selection=tree_info["branch_selection"],
                    max_events=tree_info["max_events"],
                )
                df = self.decorate_and_flatten(events, physics=tree_info["physics"], dsid = dsids[ counter ] )
                output_file = os.path.join(
                    self.config.get("output_dir", ""),
                    os.path.basename(ntuple).replace(
                        ".root", f"_{tree_info['tree_name']}.ftr"
                    ),
                )
                df.to_feather(output_file)
            self.logger.info(f"\tSaved to {output_file}!")
