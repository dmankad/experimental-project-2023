from src.data_preparation import ntuplemaker
from src.utils import ioutils

def process_ntuples(config, logger):
    input_files = [ i[ 1 ] for i in config[ "input_files" ] ]
    dsids       = [ str( i[ 0 ] ) for i in config[ "input_files" ] ] 
    ntuples = ioutils.get_files( input_files, config[ "input_dir" ] )
    ntuplemaker.NTupleMaker(config, logger=logger).run(ntuples, dsids = dsids )

