#!/usr/bin/env python3

import json
import sys
import math
import pandas as pd
import numpy as np
import os
from os import path
import logging

import dgl

import torch
# torch.set_default_tensor_type("torch.cuda.FloatTensor")
import torch.nn
import torch.optim as optim
from torch.utils.data import Dataset, DataLoader

from sklearn.metrics import confusion_matrix as skcmatrix

import matplotlib
matplotlib.use('agg')
from matplotlib import pyplot as plt

from ml_share import ml_data_preparation
from ml_share import ml_parameters
from ml_share import ml_device
from ml_share import ml_config
from ml_share import gnn_create_graphs
from ml_share import gnn_models
from ml_share import utils
from ml_share import plotting



default_config  = utils.json_load("for_gnn/config_file.json") # relative to ml_main/



logger = logging.getLogger( "Learning" )
logger.setLevel( logging.DEBUG )
ch = logging.StreamHandler()
ch.setLevel( logging.DEBUG )
ch.setFormatter( utils.CustomFormatter() )
logger.addHandler( ch )



def split(
        full_dataset,
        train_fraction  = None,
        test_fraction   = None 
    ):

    """
    [ MACHINE LEARNING ] [ CODE ]
    - Of course, even if we are using the words, "train", and "test", the same split function can be used for splitting the training dataset further into training vs. validation datasets. However, we would need a different splitter for cross validation if we choose to use cross validation.
    [ MACHINE LEARNING ] [ CODE ] [ WIP ]
    - This version of the split function is not stratified, i.e., it doesn't explicitly ensure that the split conserves the ratio of signal v/s. background. This will be implmeneted in the subsequent versions of the code.
    """

    if( train_fraction is None and test_fraction is None     ):
        train_fraction  = config[ "data_preparation" ][ "split" ][ "train_fraction" ]
    if( train_fraction is None and test_fraction is not None ):
        train_fraction  = 1 - test_fraction
    if( train_fraction is not None and test_fraction is None ):
        test_fraction   = 1 - train_fraction

    train_size                  = int(train_fraction * len(full_dataset))
    test_size                   = len(full_dataset) - train_size
    train_dataset, test_dataset = torch.utils.data.random_split(full_dataset, [train_size, test_size])

    return train_dataset, test_dataset

def cross_validation_split(
        dataset,
        cross_validation_rounds = None
        ):

    """
    [ MACHINE LEARNING ] [ CODE ] [ WIP ]
    - This version of the cross validation split function is not stratified, i.e., it doesn't explicitly ensure that the split conserves the ratio of signal v/s. background. This will be implmeneted in the subsequent versions of the code.
    """
    
    if(cross_validation_rounds is None):
        cross_validation_rounds = config[ "data_preparation" ][ "cross_validation" ][ "cross_validation_rounds" ]
    full_length = len( dataset )
    split_lengths = []
    for i in range( cross_validation_rounds - 1 ):
        split_lengths.append( int( full_length * ( 1 / float( cross_validation_rounds ) ) ) )
    split_lengths.append( full_length - np.array( split_lengths ).sum() )
    split_datasets = torch.utils.data.random_splits(dataset, split_lengths)
    training_datasets = []
    validation_datasets = []
    for i in range( cross_validation_rounds ):
        training_datasets.append( [] )
        validation_datasets.append( split_datasets[ - cross_validation_rounds ] )
        for j in range( cross_validation_rounds - 1 ):
            k = ( i + j ) % ( cross_validation_rounds )
            training_datasets[ i ] += split_datasets[ k ]
    return training_datasets, validation_datasets

def get_loss_function(
        # loss_function_name      = None,
        loss_function_name,
        weight                  = None,
        pos_weight              = torch.tensor(2).to(ml_device.device)
        ):
    # if( loss_function_name is None ):
    #     loss_function_name = default_config[ "evaluation" ][ "loss_function_name" ]

    if( loss_function_name in [ "binary_crossentropy", "BCELoss" ] ):
        if( weight is None ):
            function = torch.nn.BCELoss()
        else:
            function = torch.nn.BCELoss(weight)
    if( loss_function_name in [ "mean_absolute_error", "mae", "MAE", "L1Loss" ] ):
        function = torch.nn.L1Loss()
    if( loss_function_name in [ "MSELoss" ] ):
        function = torch.nn.MSELoss()
    if( loss_function_name in [ "BCEWithLogitsLoss" ] ):
        if( weight is None and pos_weight is None):
            function = torch.nn.BCEWithLogitsLoss()
        elif(weight is not None and pos_weight is None):
            function = torch.nn.BCEWithLogitsLoss( weight = weight )
        elif(pos_weight is not None and weight is None):
            function = torch.nn.BCEWithLogitsLoss( pos_weight = pos_weight )
    if( loss_function_name in [ "crossentropy", "CrossEntropyLoss" ] ):
        if( weight is None ):
            function = torch.nn.CrossEntropyLoss()
        else:
            function = torch.nn.CrossEntropyLoss( weight = weight )

    return function.to(ml_device.device)

def get_optimizer(
        classifier_parameters,
        learning_rate   = None,
        optimizer_name  = None
        ):
    if( learning_rate is None ):
        learning_rate = default_config[ "training" ][ "learning_rate" ]
    if( optimizer_name is None ):
        optimizer_name = default_config[ "training" ][ "optimizer_name" ]
    if( optimizer_name in [ "Adam", "adam" ] ):
        optimizer = optim.Adam( classifier_parameters, lr = learning_rate )
    return optimizer

class Evaluator:
    def __init__(
            self,
            dataset                 = None, # list of tuples
            dataloader              = None, # torch.utils.data.dataloader.DataLoader object
            classifier              = None, # trained network
            model                   = None, # architechture of the network
            state_dict              = None, # state dictionary with trained values of parameters for the given architecture of the network
            config                  = None, # dictionary
            parameters              = None,
            proba_cuts              = None, # value or list of values
            loss_function           = None, # loss function object, e.g., torch.nn.BCELoss()
            loss_function_name      = None, # string, name of the loss function
            hyperparameters         = None
        ):
        self.config = default_config[ "evaluation" ]
        self.parameters = parameters
        self.proba_cuts = list( np.round( np.linspace( 0, 1, self.config[ "proba_cuts" ][ "number" ] ), self.config[ "proba_cuts" ][ "decimals" ] ) )
        if( dataset is not None ):
            self.dataset = dataset
            self.dataloader = DataLoader(self.dataset)
            """
            [ MACHINE LEARNING ] [ CODE ] [ COMMENT ] [ DOUBT ]
            - Notice that the evaluator is only being used for validation/testing/application. So, we don't need to provide a batch size or make sure that shuffling happens.
            - But do we need a specific collate function? I am pretty sure, not. But why do we need it in the first place in the training either? The DataLoader module of pyTorch has a default collate function that it can use. So why did Jonathan give a manual collate function in the course? 
            """
        if( dataloader is not None):
            self.dataloader = dataloader
        if( dataset is None and dataloader is None):
            logger.warning("Neither a dataset nor a dataloader has been provided to the evaluator class instantiation. Certain class methods will not be able to function due to the same.")
        if( dataset is not None and dataloader is not None):
            logger.warning("Both a dataset and a dataloader have been provided to the evaluator class object. The code isn't designed to check if the provided dataloader corresponds to the provided dataset. The code will ignore the provided dataset and work with the provided dataloader.")
        if( model is not None and state_dict is not None):
            self.classifier = model
            self.classifier.load_state_dict(state_dict)
        if( model is not None and state_dict is None):
            logger.warning("")
        if( model is None and state_dict is not None):
            logger.warning("")
        if( model is not None and classifier is not None):
            logger.warning("")
        if( classifier is not None ):
            self.classifier = classifier
        else:
            logger.warning("A classifier hasn't been provided to the evaluator class instantiation. Certain class methods will not be able to function due to the same.")
        if( config is not None ):
            self.config = config
        if( proba_cuts is not None ):
            self.proba_cuts = proba_cuts
            if( isinstance( self.proba_cuts, list ) is not True ):
                self.proba_cuts = [ self.proba_cuts ]
        if( loss_function is not None):
            self.loss_function = loss_function
            if( loss_function_name is not None):
                pseudo_loss_function = get_loss_function( loss_function_name )
                if( str(pseudo_loss_function) != str(self.loss_function) ):
                    logger.warning("The evaluator class object has been provided a loss function `"+str(self.loss_function)+"` simultaneously with the loss function name corresponding to the loss function `"+str(pseudo_loss_function)+"`. The code will ignore the latter and move ahead with the provided loss function, i.e., `"+str(self.loss_function)+"`.")
        else:
            if( loss_function_name is None ):
                loss_function_name = self.config[ "loss_function_name_" + ( "multinode" if "multinode" in self.parameters.get("classification_type") else "binary" ) ]
            self.loss_function = get_loss_function( loss_function_name )






    def get_proba( self, scores, labels_scheme = None, suffix = "gnn" ):
        if( labels_scheme is None ):
            labels_scheme = self.parameters[ "labels_scheme" ]
        df_proba = pd.DataFrame()
        df_scores = pd.DataFrame( scores )
        for label in labels_scheme.keys():
            df_proba[ "proba_" + suffix + "_"  + str(label) ] = df_scores[ int(labels_scheme[ label ]) ].reset_index( drop = True )
        df_proba[ "proba_" + suffix ] = self.get_binary_scores( scores, labels_scheme )
        return df_proba


    def get_binary_scores( self, labels, labels_scheme = None ):
        if( labels_scheme is None ):
            labels_scheme = self.parameters[ "labels_scheme" ]

        if( labels_scheme is None ):
            return labels
        else:
            signal_positions = []
            for label in labels_scheme.keys():
                if str(label) in ( ml_config.electron_signal_id if self.parameters["channel"] == "electron" else ml_config.muon_signal_id ):
                    if( labels_scheme[label] not in signal_positions ):
                        signal_positions.append( labels_scheme[ label ] )
            df_labels = pd.DataFrame( labels )
            signal_scores = np.array( df_labels[ signal_positions ].reset_index(drop = True ).sum( axis = 1 ) )
            return signal_scores

    # def get_multiclass_classification( self, scores, proba_cut ):
    #     binary_scores = self.get_binary_scores( scores )
    #     sig_or_bkg = binary_scores > proba_cut
    #     df_scores = pd.DataFrame( scores )
    #     sig1 = sig_or_bkg & ( df_scores[ 1 ] >= df_scores[ 2 ] )
    #     sig2 = sig_or_bkg & ( df_scores[ 1 ] < df_scores[ 2 ] )
    #     multiclass = sig1.apply( lambda x: int(x) ) + sig2.apply( lambda x: 2*int(x) )
    #     return multiclass

    # def mass_hypo_accuracy( self, pred_scores = None, truth_scores = None, proba_cuts = None, hypo_threshold = None ):
    #     if( pred_scores is None ):
    #         pred_scores = self.preds
    #     if( truth_scores is None ):
    #         truth_scores = self.truths
    #     if( proba_cuts is None ):
    #         proba_cuts = self.proba_cuts
    #     value = {"proba_cut":{}}
    #     #signal_scores = pred_scores["300590"] + pred_scores["300591"]
    #     signal_scores = self.get_binary_scores( pred_scores )
    #     pred_scores = self.get_proba( pred_scores )
    #     truth_scores = self.get_proba( truth_scores )
    #     for proba_cutvalue in proba_cuts:
    #         signal_mask = ( signal_scores >= proba_cutvalue )
    #         bd_mask = ( pred_scores[ "300590" ] >= pred_scores[ "300591" ] ) & signal_mask
    #         bdbar_mask = ( pred_scores[ "300590" ] < pred_scores[ "300591" ] ) & signal_mask
    #         if( hypo_threshold is not None ):
    #             bd_mask = ( pred_scores[ "300590" ] >= hypo_threshold ) & bd_mask
    #             bdbar_mask = ( pred_scores["300591"] >= hypo_threshold ) & bdbar_mask
    #         truth_mass_bd = ( truth_scores[ "300590" ] == 1 ) & signal_mask & ( bd_mask | bdbar_mask )
    #         truth_mass_bdbar = ( truth_scores[ "300591" ] == 1 ) & signal_mask & ( bd_mask | bdbar_mask )
    #         n_truth_signal = truth_mass_bd.sum() + truth_mass_bdbar.sum()
    #         n_correct_mass = ( bd_mask & truth_mass_bd ).sum() + ( bdbar_mask & truth_mass_bdbar ).sum()
    #         acc = float(n_correct_mass) / float( n_truth_signal ) if n_truth_signal > 0 else 0
    #         value["proba_cut"][ str(proba_cutvalue ) ] = acc
    #     return value






    def get_confusion_matrix( self, preds = None, truths = None, proba_cuts = None, standalone = False ):
        if( proba_cuts is None ):
            try:
                proba_cuts = self.proba_cuts
            except:
                logger.error("")
        if( isinstance( proba_cuts, list ) is not True):
            proba_cuts = [ proba_cuts ]
        if( preds is None and truths is not None):
            logger.warning("Predictions are provided to the confusion_matrix() explicitly but the truths are left to be searched from the class.")
        if( truths is None and preds is not None):
            logger.warning("Truths are provided to the confusion_matrix() explicitly but the predictions are left to be searched from the class.")
        if( preds is None):
            try:
                preds = self.preds
            except:
                logger.error("")
        if( truths is None):
            try:
                truths = self.truths
            except:
                logger.error("")


        truths = self.get_binary_scores( truths )
        preds = self.get_binary_scores( preds )
        #self.truths = truths
        #self.preds = preds

        confusion_matrix = {}
        #multi_confusion_matrix = {}
        confusion_matrix[ "proba_cut" ] = {}
        #multi_confusion_matrix["proba_cut"] = {}

        for proba_cut_value in proba_cuts:
            true_positive   = ((preds >= proba_cut_value) & ( truths == 1)).sum()
            true_negative   = ((preds < proba_cut_value) & ( truths == 0)).sum()
            false_positive   = ((preds >= proba_cut_value) & ( truths == 0)).sum()
            false_negative   = ((preds < proba_cut_value) & ( truths == 1)).sum()
            confusion_matrix[ "proba_cut" ][ str(proba_cut_value) ] = {
                    "true_positive"     : true_positive,
                    "true_negative"     : true_negative,
                    "false_positive"    : false_positive,
                    "false_negative"    : false_negative
                    }
            #multi_confusion_matrix["proba_cut"][ str(proba_cut_value) ] = skcmatrix( multi_truths, multi_preds )
            #disp = ConfusionMatrixDisplay( multi_confusion_matrix["proba_cut"][ str(proba_cut_value)])
            #disp.plot()
            #plt.savefig("confusion_matrix.pdf")
            #plt.clf()

        if( standalone == False ):
            self.confusion_matrix = confusion_matrix
            #self.multi_confusion_matrix = multi_confusion_matrix

        return confusion_matrix







    def recall(
            self,
            proba_cuts          = None,
            preds               = None,
            truths              = None,
            true_positive       = None,
            true_negative       = None,
            false_positive      = None,
            false_negative      = None,
            confusion_matrix    = None,
            standalone          = False
        ):
        if( proba_cuts is None ):
            try:
                proba_cuts = self.proba_cuts
            except:
                """
                [ CODE ] [ WIP ]
                - Need to put appropriate logger outputs corresponding to different scenarios.
                """
                pass
        if( isinstance( proba_cuts, list) is not True ):
            proba_cuts = [ proba_cuts ]
        if( truths is None and preds is None ):
            if( standalone is False):
                # try:
                #     confusion_matrix = self.confusion_matrix
                # except:
                confusion_matrix = self.get_confusion_matrix( proba_cuts = proba_cuts, preds = preds, truths = truths, standalone = standalone )
        value = {}
        value[ "proba_cut" ] = {}
        if( confusion_matrix is not None):
            if( "proba_cut" in confusion_matrix.keys()):
                for proba_cut_value in confusion_matrix["proba_cut"].keys():
                    true_positive   = confusion_matrix["proba_cut"][ proba_cut_value ][ "true_positive" ]
                    true_negative   = confusion_matrix["proba_cut"][ proba_cut_value ][ "true_negative" ]
                    false_positive  = confusion_matrix["proba_cut"][ proba_cut_value ][ "false_positive" ]
                    false_negative  = confusion_matrix["proba_cut"][ proba_cut_value ][ "false_negative" ]
                    value[ "proba_cut" ][ proba_cut_value ] = true_positive / ( true_positive + false_negative )
            else:
                true_positive       = confusion_matrix[ "true_positive" ]
                true_negative       = confusion_matrix[ "true_negative" ]
                false_positive      = confusion_matrix[ "false_positive" ]
                false_negative      = confusion_matrix[ "false_negative" ]
                value =  true_positive / ( true_positive + false_negative )
        else:
            if( true_positive is not None and true_negative is not None and false_positive is not None and false_negative is not None):
                value =  true_positive / ( true_positive + false_negative )
        
        return value

    def precision(
            self,
            proba_cuts           = None,
            preds               = None,
            truths              = None,
            true_positive       = None,
            true_negative       = None,
            false_positive      = None,
            false_negative      = None,
            confusion_matrix    = None
        ):
        if( proba_cuts is None ):
            try:
                proba_cuts = self.proba_cuts
            except:
                """
                [ CODE ] [ WIP ]
                - Need to put appropriate logger outputs corresponding to different scenarios.
                """
                pass
        if( isinstance( proba_cuts, list) is not True ):
            proba_cuts = [ proba_cuts ]
        if( truths is None or preds is None ):
            confusion_matrix = self.get_confusion_matrix( proba_cuts = proba_cuts, preds = preds, truths = truths, standalone = True )
        value = {}
        value[ "proba_cut" ] = {}
        if( confusion_matrix is not None):
            if( "proba_cut" in confusion_matrix.keys()):
                for proba_cut_value in confusion_matrix["proba_cut"].keys():
                    true_positive  = confusion_matrix["proba_cut"][ proba_cut_value ][ "true_positive" ]
                    true_negative  = confusion_matrix["proba_cut"][ proba_cut_value ][ "true_negative" ]
                    false_positive = confusion_matrix["proba_cut"][ proba_cut_value ][ "false_positive" ]
                    false_negative = confusion_matrix["proba_cut"][ proba_cut_value ][ "false_negative" ]
                    if true_positive == 0 and false_positive == 0:
                        value[ "proba_cut" ][ proba_cut_value ] = 0
                    else:
                        value[ "proba_cut" ][ proba_cut_value ] = true_positive / ( true_positive + false_positive )
            else:
                true_positive  = confusion_matrix[ "true_positive" ]
                true_negative  = confusion_matrix[ "true_negative" ]
                false_positive = confusion_matrix[ "false_positive" ]
                false_negative = confusion_matrix[ "false_negative" ]
                if true_positive == 0 and false_positive == 0:
                    value = 0
                else:
                    value = true_positive / ( true_positive + false_positive )
        else:
            if( true_positive is not None and true_negative is not None and false_positive is not None and false_negative is not None):
                value =  true_positive / ( true_positive + false_positive )

        return value

    def f_score(
            self,
            beta                = None,
            proba_cuts           = None,
            preds               = None,
            truths              = None,
            true_positive       = None,
            true_negative       = None,
            false_positive      = None,
            false_negative      = None,
            confusion_matrix    = None
        ):
        if(beta is None):
            beta = self.config["metrics"]["f_score"]["beta"]
        if( proba_cuts is None ):
            try:
                proba_cuts = self.proba_cuts
            except:
                """
                [ CODE ] [ WIP ] 
                - Need to put appropriate logger outputs corresponding to different scenarios.
                """
                pass
        if( isinstance( proba_cuts, list) is not True ):
            proba_cuts = [ proba_cuts ]
        if( truths is None or preds is None ):
            confusion_matrix = self.get_confusion_matrix( proba_cuts = proba_cuts, preds = preds, truths = truths, standalone = True )
        value = {}
        value[ "proba_cut" ] = {}
        if( confusion_matrix is not None):
            if( "proba_cut" in confusion_matrix.keys()):
                for proba_cut_value in confusion_matrix["proba_cut"].keys():
                    true_positive   = confusion_matrix["proba_cut"][ proba_cut_value ][ "true_positive" ]
                    true_negative   = confusion_matrix["proba_cut"][ proba_cut_value ][ "true_negative" ]
                    false_positive  = confusion_matrix["proba_cut"][ proba_cut_value ][ "false_positive" ]
                    false_negative  = confusion_matrix["proba_cut"][ proba_cut_value ][ "false_negative" ]
                    try:
                        value[ "proba_cut" ][ proba_cut_value ] =  ( ( 1 + beta * beta ) * true_positive  ) / ( ( 1 + beta * beta ) * true_positive + beta * beta * false_negative + false_positive )
                    except:
                        value[ "proba_cut" ][ proba_cut_value ] = -1
            else:
                true_positive       = confusion_matrix[ "true_positive" ]
                true_negative       = confusion_matrix[ "true_negative" ]
                false_positive      = confusion_matrix[ "false_positive" ]
                false_negative      = confusion_matrix[ "false_negative" ]
                try:
                    value =  ( ( 1 + beta * beta ) * true_positive  ) / ( ( 1 + beta * beta ) * true_positive + beta * beta * false_negative + false_positive )
                except:
                    value = -1
        else:
            if( true_positive is not None and true_negative is not None and false_positive is not None and false_negative is not None):
                try:
                    value =  ( ( 1 + beta * beta ) * true_positive  ) / ( ( 1 + beta * beta ) * true_positive + beta * beta * false_negative + false_positive )
                except:
                    value = -1

        return value

    def accuracy(
            self,
            proba_cuts          = None,
            preds               = None,
            truths              = None,
            true_positive       = None,
            true_negative       = None,
            false_positive      = None,
            false_negative      = None,
            confusion_matrix    = None
        ):
        if( proba_cuts is None ):
            try:
                proba_cuts = self.proba_cuts
            except:
                # [ WIP ] Need to put appropriate logger outputs corresponding to different scenarios.
                pass
        if( isinstance( proba_cuts, list) is not True ):
            proba_cuts = [ proba_cuts ]
        if( truths is None or preds is None ):
            confusion_matrix = self.get_confusion_matrix( proba_cuts = proba_cuts, preds = preds, truths = truths, standalone = True )
        value = {}
        value[ "proba_cut" ] = {}
        if( confusion_matrix is not None):
            if( "proba_cut" in confusion_matrix.keys()):
                for proba_cut_value in confusion_matrix["proba_cut"].keys():
                    true_positive   = confusion_matrix["proba_cut"][ proba_cut_value ][ "true_positive" ]
                    true_negative   = confusion_matrix["proba_cut"][ proba_cut_value ][ "true_negative" ]
                    false_positive  = confusion_matrix["proba_cut"][ proba_cut_value ][ "false_positive" ]
                    false_negative  = confusion_matrix["proba_cut"][ proba_cut_value ][ "false_negative" ]
                    try:
                        value[ "proba_cut" ][ proba_cut_value ] = ( true_positive + true_negative ) / ( true_positive + true_negative + false_positive + false_negative )
                    except:
                        value[ "proba_cut" ][ proba_cut_value ] = -1
            else:
                true_positive       = confusion_matrix[ "true_positive" ]
                true_negative       = confusion_matrix[ "true_negative" ]
                false_positive      = confusion_matrix[ "false_positive" ]
                false_negative      = confusion_matrix[ "false_negative" ]
                try:
                    value =  ( true_positive + true_negative ) / ( true_positive + true_negative + false_positive + false_negative )
                except:
                    value = -1
        else:
            if( true_positive is not None and true_negative is not None and false_positive is not None and false_negative is not None):
                try:
                    value = ( true_positive + true_negative ) / ( true_positive + true_negative + false_positive + false_negative )
                except:
                    value = -1

        return value

    def background_rejection(
            self,
            proba_cuts          = None,
            preds               = None,
            truths              = None,
            true_positive       = None,
            true_negative       = None,
            false_positive      = None,
            false_negative      = None,
            confusion_matrix    = None
        ):
        if( proba_cuts is None ):
            try:
                proba_cuts = self.proba_cuts
            except:
                # [ WIP ] Need to put appropriate logger outputs corresponding to different scenarios.
                pass
        if( isinstance( proba_cuts, list) is not True ):
            proba_cuts = [ proba_cuts ]
        if( truths is None or preds is None ):
            confusion_matrix = self.get_confusion_matrix( proba_cuts = proba_cuts, preds = preds, truths = truths, standalone = True )
        value = {}
        value[ "proba_cut" ] = {}
        if( confusion_matrix is not None):
            if( "proba_cut" in confusion_matrix.keys()):
                for proba_cut_value in confusion_matrix["proba_cut"].keys():
                    true_positive   = confusion_matrix["proba_cut"][ proba_cut_value ][ "true_positive" ]
                    true_negative   = confusion_matrix["proba_cut"][ proba_cut_value ][ "true_negative" ]
                    false_positive  = confusion_matrix["proba_cut"][ proba_cut_value ][ "false_positive" ]
                    false_negative  = confusion_matrix["proba_cut"][ proba_cut_value ][ "false_negative" ]
                    value[ "proba_cut" ][ proba_cut_value ] = true_negative / ( true_negative + false_positive )
            else:
                true_positive       = confusion_matrix[ "true_positive" ]
                true_negative       = confusion_matrix[ "true_negative" ]
                false_positive      = confusion_matrix[ "false_positive" ]
                false_negative      = confusion_matrix[ "false_negative" ]
                value =  true_negative / ( true_negative + false_positive )
        else:
            if( true_positive is not None and true_negative is not None and false_positive is not None and false_negative is not None):
                value =  true_negative / ( true_negative + false_positive )

        return value

    def signal_efficiency(
            self,
            proba_cuts          = None,
            preds               = None,
            truths              = None,
            true_positive       = None,
            true_negative       = None,
            false_positive      = None,
            false_negative      = None,
            confusion_matrix    = None
        ):
        value = self.recall(
                proba_cuts = proba_cuts,
                preds = preds,
                truths = truths,
                true_positive = true_positive,
                true_negative = true_negative,
                false_positive = false_positive,
                false_negative = false_negative,
                confusion_matrix = confusion_matrix
                )
        return value

    def get_metrics(
            self,
            compute_metrics = None,
            ignore_metrics = None,
            compute_veto = None,
            ignore_veto = None
        ):
        if(compute_veto is None and ignore_veto is None):
            compute_veto = self.config[ "metrics" ][ "compute_veto" ]
            ignore_veto = self.config[ "metrics" ][ "ignore_veto" ]
        if(compute_veto is None and ignore_veto is not None):
            compute_veto = not ignore_veto
        if(compute_veto is not None and ignore_veto is None):
            ignore_veto = not compute_veto
        if(compute_veto and ignore_veto is True):
            logging.error(
                    "Incompatible configuration provided to the metricist object regarding compute_veto and ignore_veto. It is hardcoded in the propramme to use compute_veto in such a situation. Abort the program if you do not want that to happen and fix the configuration provided to the metricist object."
                    )
        if(compute_metrics is None):
            compute_metrics = self.config[ "metrics" ][ "metrics_list" ]
            #compute_metrics = ["loss", "recall", "precision", "background_rejection", "accuracy", "signal_efficiency"]
            if(ignore_metrics is not None):
                for ignore_metric in ignore_metrics:
                    compute_metrics.remove(ignore_metric)
        else:
            if(ignore_metrics is not None):
                for ignore_metric in ignore_metrics:
                    if(ignore_metric in compute_metric):
                        if(compute_veto is False):
                            compute_metrics.remove(ignore_metric)
        metrics_dict = {}

        if("recall" in compute_metrics): metrics_dict["recall"] = self.recall()
        if("accuracy" in compute_metrics): metrics_dict["accuracy"] = self.accuracy()
        if("precision" in compute_metrics): metrics_dict["precision"] = self.precision()
        if("signal_efficiency" in compute_metrics): metrics_dict["signal_efficiency"] = self.signal_efficiency()
        if("background_rejection" in compute_metrics): metrics_dict["background_rejection"] = self.background_rejection()
        if("loss" in compute_metrics): metrics_dict["loss"] = self.loss
        if("f_score" in compute_metrics): metrics_dict["f_score"] = self.f_score()
        #if("mass_hypo_accuracy" in compute_metrics): metrics_dict["mass_hypo_accuracy"] = self.mass_hypo_accuracy( hypo_threshold = None )
        #if("mass_hypo_accuracy_70" in compute_metrics): metrics_dict["mass_hypo_accuracy_70"] = self.mass_hypo_accuracy( hypo_threshold = 0.7 )
        #if("mass_hypo_accuracy_80" in compute_metrics): metrics_dict["mass_hypo_accuracy_80"] = self.mass_hypo_accuracy( hypo_threshold = 0.8 )
        #if("mass_hypo_accuracy_90" in compute_metrics): metrics_dict["mass_hypo_accuracy_90"] = self.mass_hypo_accuracy( hypo_threshold = 0.9 )

        return metrics_dict

    def evaluate(
            self,
            dataset = None,
            dataloader = None,
            classifier = None,
            model = None,
            state_dict = None,
            standalone = False,
            suffix    = "",
        ):
        """
        [ MACHINE LEARNING ] [ EXPLANATION ]
        - It is important to put the network into .eval() mode in order to make sure that certain layers are turned off that we want to be turned off during evaluation, for example, BatchNorm and Dropout layers. ###
        - We futher pair this with using torch.no_grad() as we will do below because we don't need to evaluate gradients while evaluating the model, we only need the gradients if we want to backpropagate which we, by definition, don't do while evaluating a model. Notice that we are using .no_grad() in a block. This ensures that the gradients are turned back on as soon as we are out of the block. ### 
        """

        is_multinode = "multinode" in self.parameters.get("classification_type")

        if( dataloader is None and dataset is None ):
            dataloader = self.dataloader
        if( dataset is not None and dataloader is None ):
            dataloader = DataLoader( dataset, shuffle = False )
        if( dataset is not None and dataloader is not None ):
            logger.warning("")
        if( classifier is None ):
            if( model is None or state_dict is None):
                classifier = self.classifier
            else:
                classifier = model.load_state_dict( state_dict )
        classifier.eval()
        all_truths  = []
        all_preds   = []
        n_batches   = 0
        loss = 0
        with torch.no_grad():
            for batched_graphs, batched_labels in dataloader:
                n_batches       += 1
                if is_multinode:
                    truths          = batched_labels.float() # removing the unsuqeeze(-1) # the float is needed for BCE loss, long is needed for CELoss.
                else:
                    truths          = batched_labels.unsqueeze(-1).float()
                preds           = classifier(batched_graphs)
                loss            += self.loss_function( preds , truths ).item()
                all_truths.append(truths)
                all_preds.append(preds)
        loss = loss / n_batches
        all_truths = torch.cat(all_truths).to("cpu").numpy() # [ WIP ] Resolve the numpy deprication warning.
        all_preds  = torch.cat(all_preds).to("cpu").numpy() # [ WIP ] Resolve the numpy deprication warning.
        if( standalone is False ):
            self.dataloader = dataloader
            self.classifier = classifier
            self.truths = all_truths
            self.preds = all_preds
            self.loss = loss

        """
        [ MACHINE LEARNING ] [ COMMENT ] [ WIP ]
         - Notice how the loss is calculated, in particular, where it is calculated. It is being calculated along with true/false positive/negatives. On the other hand, the performance metrics are computed after we have an aggregate data of the true/false positive/negative. In other words, the loss function doesn't directly get the information about the overall trend of the behavior of the model, it is very local, it only calculates how well or badly a given prediction did. I am wondering if there is any way (if it is even useful) to use the global information available in the computation metrics in the computation of the loss function.
        """

        if is_multinode:
            return self.get_proba( all_preds, suffix = suffix ), self.get_proba( all_truths, suffix = suffix )
        else:
            return pd.DataFrame(all_preds, columns = ["proba_" + suffix]), pd.DataFrame(all_truths, columns = ["true_signal"])



class Trainer():
    def __init__(
            self,
            dataset                 = None,
            batch_size              = None,
            collate_function        = None,
            dataloader              = None,
            model                   = None,
            state_dict              = None,
            classifier              = None,
            n_epochs                = None,
            loss_function           = None,
            loss_function_name      = None,
            weight                  = None,
            pos_weight              = None,
            cross_validate          = None,
            optimizer_name          = None,
            learning_rate           = None,
            validation_fraction     = None,
            cross_validation_rounds = None,
            proba_cuts              = None,
            config                    = None,
            parameters                = None,
            dir_name = ""):
        self.DIR_NAME = dir_name



        if( config is None ):
            self.config                 = default_config["training"]
        else:
            self.config                = config
        self.parameters = parameters
        # self.classifier             = gnn_models.get_classifier(self.config["classifier_name"])
        hyperparameters = {}
        hypernames = [ "num_node_features", "num_edge_features", "num_goblin_features", "num_nodes", "num_edges" ]
        for hypername in hypernames:
            if hypername in parameters.keys():
                hyperparameters[ hypername ] = parameters[ hypername ]
        hyperparameters[ "goblins_infect_broadcast" ] = "infect_broadcast" in parameters[ "goblins_plan" ]
        if len( hyperparameters.keys() ) == 0:
            hyperparameters = None
        self.hyperparameters = hyperparameters
        self.classifier             = gnn_models.get_classifier(parameters["classifier_name"], hyperparameters )


        self.optimizer_name         = optimizer_name
        if(self.optimizer_name is None):
            self.optimizer_name     = self.config["optimizer_name"]
        self.learning_rate          = learning_rate
        if(learning_rate is None):
            self.learning_rate      = self.config["learning_rate"]
        self.optimizer              = get_optimizer(self.classifier.parameters(), self.learning_rate, self.optimizer_name)


        self.batch_size  = batch_size
        if( self.batch_size is None):
            self.batch_size = self.config["batch_size"]
        if( collate_function is not None ):
            self.collate_function = collate_function
        ### Notice that the collate function is necesssary to be provided because the default collate function of pytorch doesn't work with graphs. But, for a non-graph neural network, we don't need to provide it with a custom collate function, the default collate function of pytorch would work just fine.

        if( dataset is not None ):
            self.dataset            = dataset
        if( dataloader is not None ):
            self.dataloader         = dataloader
        else:
            if( self.dataset is not None ):
                self.dataloader = DataLoader( dataset = self.dataset, batch_size = self.batch_size, collate_fn = self.collate_function )
        if( dataset is None and dataloader is None ):
            logger.warning()
        if( dataset is not None and dataloader is not None ):
            logger.warning()

        self.weight = weight
        """
        if(self.weight is True):
            for i, j in dataloader:
                break
            frequency_dict = {}
            for label in j:
                if(str(label) not in frequency_dict.keys()):
                    frequency_dict[str(label)] = 1
                else:
                    frequency_dict[str(label)] += 1
        """
        if(isinstance(self.weight, int) or isinstance(self.weight, float)):
            self.weight = self.weight * torch.ones(self.batch_size)

        if( loss_function is not None):
            self.loss_function = loss_function
        else:
            if( loss_function_name is not None):
                self.loss_function_name = loss_function_name
                if(self.weight is True):
                    self.loss_function  = get_loss_function(self.loss_function_name, weight = self.weight)
                else:
                    self.loss_function  = get_loss_function(self.loss_function_name)
            else:
                self.loss_function_name  = self.config["loss_function_name_" + ( "multinode" if "multinode" in self.parameters.get("classification_type") else "binary" )]
                if(self.weight is True):
                    self.loss_function  = get_loss_function(self.loss_function_name, weight = self.weight)
                else:
                    self.loss_function  = get_loss_function(self.loss_function_name)

        self.validation_fraction    = validation_fraction
        if( self.validation_fraction is None ):
            self.validation_fraction = self.config["validation_fraction"]
        self.cross_validate         = cross_validate
        if( self.cross_validate is None ):
            self.cross_validate = bool(self.config["cross_validate"])

        self.proba_cuts             = proba_cuts
        if(self.proba_cuts is None):
            self.proba_cuts = list(np.round(np.linspace(0, 1, self.config["proba_cuts"][ "number" ] ), self.config["proba_cuts"]["decimals"]))

        self.n_epochs = n_epochs
        if(self.n_epochs is None):
            self.n_epochs = self.config["epochs"]

        if( classifier is not None):
            self.classifier = classifier
        else:
            if( model is not None and state_dict is not None):
                self.classifier = model.load_state_dict(state_dict)
        if( classifier is not None and model is not None and state_dict is not None):
            logger.warning()

    def train_cross_validate(self):
        training_datasets, validation_datasets = cross_validation_split(self.dataloader, cross_validation_rounds)
        for cross_validation_index in range(cross_validation_rounds):
            logging.info("Cross-Validation: "+str(cross_validation_index)+" out of "+str(cross_validation_rounds)+".")
            training_dataset = training_datasets[cross_validation_index]
            validation_dataset = validation_datasets[cross_validation_index]
            for epoch in range(n_epochs):
                train(cross_validate = False)

    def train(self, n_epochs = None, cross_validate = None, training_dataset = None, validation_dataset = None, update_broadcast = True, standalone = False):
        is_multinode = "multinode" in self.parameters.get("classification_type")

        if(n_epochs is None):
            n_epochs = self.n_epochs
        if(cross_validate is None):
            cross_validate = self.cross_validate
            if(training_dataset is None and validation_dataset is None):
                training_dataset, validation_dataset = split(self.dataset, test_fraction = self.validation_fraction)
                training_dataset = DataLoader(training_dataset, batch_size = self.batch_size, shuffle = True, collate_fn = gnn_create_graphs.collate_graphs)
                validation_dataset = DataLoader(validation_dataset, batch_size = self.batch_size, shuffle = True, collate_fn = gnn_create_graphs.collate_graphs)
            if(standalone is True):
                self.training_dataset = training_dataset
                self.validation_dataset = validation_dataset

        if( cross_validate == True ):
            self.train_cross_validate() 
        else:
            self.training_metrics = []
            self.validation_metrics = []
            self.classifier.train()
            for epoch in range(n_epochs):
                self.loss = 0
                logger.info("Epoch: "+str(epoch+1)+"/"+str(n_epochs))
                n_batches = 0
                for batched_graphs, batched_labels in training_dataset:
                    self.optimizer.zero_grad()
                    if is_multinode:
                        truths = batched_labels.float() # removing the unsqueeze(-1)
                    else:
                        truths = batched_labels.unsqueeze(-1).float()
                    preds = self.classifier(batched_graphs)
                    loss = self.loss_function( preds, truths )
                    self.loss += loss
                    loss.backward()
                    self.optimizer.step()
                    n_batches += 1
                    ### MACHINE LEARNING COMMENT ###
                    ### The above two steps are ridicuously confusing. In particular, you see, the loss object is just defined independently of the network, it is not defined as net.something but simply as torch.nn.something. Thankfully, the optimizer is defined with reference to the network, in particular, the optimizer object's initialization takes the network as an argument. But, now, how does the optimizer know where to find the loss function while performing the step()? Intuitively, one would imagine that we should be doing something like self.optimizer.step(loss) to let it know which loss to consider! However, apparently, pyTorch takes care of letting the network know of the loss function behind the scenes -- in particular, when we call the loss.backward(), the loss function finds the network and updates the gradients. This, in turn, lets the optimizer know how to perform step(). See, https://stackoverflow.com/q/53975717/8792904. No single answer is wholly satisfying but all answers taken together give a fairly good idea of what is happening. ###
                    ################################
                self.loss = self.loss.item()/n_batches

                self.training_evaluator   = Evaluator(dataloader = training_dataset,   classifier = self.classifier, proba_cuts = self.proba_cuts, parameters = self.parameters, hyperparameters = self.hyperparameters )
                self.validation_evaluator = Evaluator(dataloader = validation_dataset, classifier = self.classifier, proba_cuts = self.proba_cuts, parameters = self.parameters, hyperparameters = self.hyperparameters )

                # df_pred_proba, df_truth_proba = self.validation_evaluator.evaluate(is_multinode = is_multinode)
                self.validation_evaluator.evaluate()
                self.training_evaluator.evaluate()

                training_metrics = self.training_evaluator.get_metrics()
                training_metrics["loss"] = self.loss
                self.training_metrics.append(training_metrics)
                validation_metrics = self.validation_evaluator.get_metrics()
                self.validation_metrics.append(validation_metrics)
                if(update_broadcast is True):
                    self.temp_plotter = Plotter(self.validation_metrics, self.training_metrics, self.DIR_NAME)
                    self.temp_plotter.plot_metrics_over_epochs(["loss", "accuracy"], self.proba_cuts[math.floor(len(self.proba_cuts)/2)])
                    # self.temp_plotter.plot_response_curve(self.validation_evaluator.preds, self.validation_evaluator.truths)
                    # self.temp_plotter.plot_response_curve( df_pred_proba, df_truth_proba )
                    # self.temp_plotter.plot_response_curve( self.validation_evaluator.get_binary_scores( df_pred_proba), self.validation_evaluator.get_binary_scores(df_truth_proba ))
                    self.temp_plotter.plot_metrics_over_proba_cuts(["signal_efficiency", "background_rejection"])
                    self.temp_plotter.plot_parametric_curve(["signal_efficiency", "precision"], ["background_rejection", "recall"])
                    del self.temp_plotter

                if len(self.validation_metrics)==1 or self.validation_metrics[-2]["loss"] > self.validation_metrics[-1]["loss"]:
                    torch.save(self.classifier.state_dict(), path.join(self.DIR_NAME,'trained_model.pt'))
                history = {}
                history["training_history"] = self.training_metrics
                history["validation_history"] = self.validation_metrics
                with open(path.join(self.DIR_NAME,"ml_history.json"), "w") as f:
                    json.dump(history, f, indent = 4)
        return self.training_metrics, self.validation_metrics

class Plotter:

    def __init__(self, validation_metrics, training_metrics = None, dir_name = ""):
        self.DIR_NAME = dir_name

        if(training_metrics is not None):
            self.training_metrics = training_metrics
        self.validation_metrics = validation_metrics
        if(isinstance(self.validation_metrics, list) is not True):
            self.validation_metrics = [self.validation_metrics]

    def plot_arrays(self, yarrays, legends, axes, xarrays = None, name = None):
        fig = plt.figure()
        fig.patch.set_facecolor('white')
        fig.patch.set_alpha(0.7)
        ax = fig.add_subplot(111)
        ax.patch.set_facecolor('white')
        ax.patch.set_alpha(1.0)
        counter = 0
        for array in yarrays:
            if(xarrays is None):
                plt.plot( np.array(yarrays[ counter ]), label = legends [counter])
            else:
                plt.plot(np.array(xarrays[ counter ]), np.array( yarrays[counter]), label = legends[ counter ])
            counter += 1 
        plt.xlabel(axes[0])
        plt.ylabel(axes[1])

        plt.legend()
        plt.savefig(name, transparent = False)
        plt.close(fig)

    def plot_metrics_over_epochs(self, metric_names, proba_cut_value = None):
        yarrays = []
        legends = []
        plot_name = ""
        if( isinstance(metric_names, list)  is False ):
            metric_names = [ metric_names ]
        for metric_name in metric_names:
            yarrays.append([])
            yarrays.append([])
            legends.append("training_"+metric_name)
            legends.append("validation_"+metric_name)
            plot_name += metric_name + "_"
            for epoch_training_metrics in self.training_metrics:
                if( proba_cut_value is None ):
                    yarrays[-2].append( epoch_training_metrics[metric_name] )
                elif( isinstance(proba_cut_value, list) is False ):
                    if( isinstance(epoch_training_metrics[metric_name], float)):
                        yarrays[-2].append( epoch_training_metrics[metric_name] )
                    else:
                        yarrays[-2].append( epoch_training_metrics[ metric_name ][ "proba_cut" ][ str( proba_cut_value ) ] )
            for epoch_validation_metrics in self.validation_metrics:
                if(proba_cut_value is None):
                    yarrays[-1].append(epoch_validation_metrics[metric_name])
                elif( isinstance(proba_cut_value, list) is False ):
                    if( isinstance(epoch_validation_metrics[metric_name], float)):
                        yarrays[-1].append(epoch_validation_metrics[metric_name])
                    else:
                        yarrays[-1].append( epoch_validation_metrics[ metric_name ][ "proba_cut" ][ str( proba_cut_value ) ] )
        plot_name = path.join(self.DIR_NAME, plot_name + "over_epochs.pdf")
        self.plot_arrays( yarrays, legends = legends, axes = ["epochs", "value"], name = plot_name)

    def plot_metrics_over_proba_cuts(self, metric_names, return_arrays = False, plot = True):
        metrics = {}
        for metric_name in metric_names:
            proba_cuts = []
            values = []
            for proba_cut_value in self.validation_metrics[-1][metric_name]["proba_cut"].keys():
                proba_cuts.append(float(proba_cut_value))
                values.append(self.validation_metrics[-1][metric_name]["proba_cut"][proba_cut_value])
            metrics[metric_name] = {"x": proba_cuts, "y": values}
        legends = []
        xarrays = []
        yarrays = []
        plot_name = ""
        for metric_name in metrics.keys():
            plot_name += metric_name + "_"
            legends.append(metric_name)
            xarrays.append(metrics[metric_name]["x"])
            yarrays.append(metrics[metric_name]["y"])
        if(plot is True):
            plot_name += "against_proba_cuts.pdf"
            axes = ["probability cut", "value"]
            plot_name = path.join(self.DIR_NAME, plot_name)
            self.plot_arrays(yarrays = yarrays, legends = legends, axes = axes, xarrays = xarrays, name = plot_name)
        if(return_arrays is True):
            return yarrays



    # def plot_response_curve(self, preds, truths):
    #     signal_mask = (truths == 1)
    #     background_mask = (truths == 0)
    #     signal_preds = preds[ signal_mask ]
    #     background_preds = preds[ background_mask ]
    #     plt.cla()
    #     plt.hist( background_preds, list(np.linspace(0,1,100)), color = "red", alpha = 0.5, label = "background candidates" )
    #     plt.hist( signal_preds, list(np.linspace(0,1,100)), color = "green", alpha = 0.5, label = "signal candidates" )
    #     plt.xlabel( "predicted signal probability" )
    #     plt.ylabel( "number of candidates" )
    #     plt.yscale("log")
    #     plt.legend()
    #     plt.savefig(path.join(self.DIR_NAME,"response_curve.pdf"), transparent = False)

    def plot_response_curve(self, preds, truths ):
        signal_Bd_mask = ( truths[ "300590" ] == 1 )
        signal_Bdbar_mask = ( truths[ "300591" ] == 1 )
        background_mask = (truths[ "data"] == 1 )
        signal_Bd_preds = (preds[ "300590" ] + preds["300591"])[ signal_Bd_mask ]
        signal_Bdbar_preds = (preds[ "300590" ] + preds["300591"])[ signal_Bdbar_mask ]
        #signal_Bdbar_preds = preds[ "300591" ][ signal_Bdbar_mask ]
        background_preds = preds[ "data" ][ background_mask ]
        signal_Bd_preds_Bd = preds[ "300590" ][ signal_Bd_mask ]
        signal_Bd_preds_Bdbar = preds[ "300591" ][ signal_Bd_mask ]
        signal_Bdbar_preds_Bd = preds[ "300590" ][ signal_Bdbar_mask ]
        signal_Bdbar_preds_Bdbar = preds[ "300591" ][ signal_Bdbar_mask ]
        background_preds_Bd = preds[ "300590" ][ background_mask ]
        background_preds_Bdbar = preds[ "300591" ][ background_mask ]
        signal_mask = signal_Bd_mask + signal_Bdbar_mask 
        signal_preds = (preds["300590"] + preds["300591"])[signal_mask]
        plt.clf()
        fig, axs, lenX, lenY = plotting.create_subplots( 3, "multiclass response curve" )
        h = axs[ 0 , 0 ].hist2d(
            signal_Bd_preds_Bd,
            signal_Bd_preds_Bdbar,
            bins = list(np.linspace(0,1,10)),
            density = True,
            norm = matplotlib.colors.LogNorm(),
            alpha = 0.5,
            cmap = plt.cm.Reds
        )
        axs[ 0, 0 ].set_xlabel( "predicted proba to be Bd", loc = "right", fontsize = 12)
        axs[ 0, 0 ].set_ylabel( "predicted proba to be Bdbar", loc = "top", fontsize = 12)
        axs[ 0, 0 ].set_title( "true label: signal Bd", fontsize = 12 )
        axs[ 0, 0 ].set_aspect( "equal", adjustable = "box" )
        fig.colorbar( h[3], ax = axs[ 0, 0 ]).set_label( "proba value", loc = "top", fontsize = 12)
        
        h = axs[ 0 , 1 ].hist2d(
            signal_Bdbar_preds_Bd,
            signal_Bdbar_preds_Bdbar,
            bins = list(np.linspace(0,1,10)),
            density = True,
            norm = matplotlib.colors.LogNorm(),
            alpha = 0.5,
            cmap = plt.cm.Reds
        )
        axs[ 0, 1 ].set_xlabel( "predicted proba to be Bd", loc = "right", fontsize = 12)
        axs[ 0, 1 ].set_ylabel( "predicted proba to be Bdbar", loc = "top", fontsize = 12)
        axs[ 0, 1 ].set_title( "true label: signal Bdbar", fontsize = 12 )
        axs[ 0, 1 ].set_aspect( "equal", adjustable = "box" )
        fig.colorbar( h[3], ax = axs[ 0, 1 ]).set_label( "proba value", loc = "top", fontsize = 12)

        h = axs[ 0 , 2 ].hist2d(
            background_preds_Bd,
            background_preds_Bdbar,
            bins = list(np.linspace(0,1,10)),
            density = True,
            norm = matplotlib.colors.LogNorm(),
            alpha = 0.5,
            cmap = plt.cm.BuPu
        )
        axs[ 0, 2 ].set_xlabel( "predicted proba to be Bd", loc = "right", fontsize = 12)
        axs[ 0, 2 ].set_ylabel( "predicted proba to be Bdbar", loc = "top", fontsize = 12)
        axs[ 0, 2 ].set_title( "true label: background", fontsize = 12 )
        axs[ 0, 2 ].set_aspect( "equal", adjustable = "box" )
        fig.colorbar( h[3], ax = axs[ 0, 2 ]).set_label( "proba value", loc = "top", fontsize = 12)

        plt.savefig(path.join(self.DIR_NAME,"response_curve_multiclass.pdf"), transparent = False)
        plt.clf()
        plt.hist( background_preds, list(np.linspace(0,1,100)), color = "red", alpha = 0.5, label = "background candidates" )
        plt.hist( signal_Bd_preds_Bd, list(np.linspace(0,1,100)), color = "green", alpha = 0.5, label = "signal Bd candidates" )
        plt.hist( signal_Bdbar_preds_Bdbar, list(np.linspace(0,1,100)), color = "blue", alpha = 0.5, label = "signal Bdbar candidates" )
        plt.xlabel( "probability to be X for X" )
        plt.ylabel( "number of candidates" )
        plt.yscale("log")
        plt.legend()
        plt.savefig(path.join(self.DIR_NAME,"response_curve.pdf"), transparent = False)
        plt.clf()
        """
        plt.hist( background_preds, list(np.linspace(0,1,100)), color = "red", alpha = 0.5, label = "background candidates" )
        plt.hist( signal_Bd_preds_Bd, list(np.linspace(0,1,100)), color = "green", alpha = 0.5, label = "signal Bd candidates" )
        plt.hist( signal_Bdbar_preds_Bdbar, list(np.linspace(0,1,100)), color = "blue", alpha = 0.5, label = "signal Bdbar candidates" )
        plt.xlabel( "probability to be X for X" )
        plt.ylabel( "number of candidates" )
        plt.yscale("log")
        plt.legend()
        plt.savefig(path.join(self.DIR_NAME,"response_curve.pdf"), transparent = False)
        plt.clf()
        """

        plt.hist( 1 - background_preds, list(np.linspace(0,1,100)), color = "red", alpha = 0.5, label = "background candidates", density = True )
        plt.hist( signal_Bd_preds, list(np.linspace(0,1,100)), color = "green", alpha = 0.5, label = "signal Bd candidates", density = True )
        plt.hist( signal_Bdbar_preds, list(np.linspace(0,1,100)), color = "blue", alpha = 0.5, label = "signal Bdbar candidates", density = True )
        plt.xlabel( "signal probability" )
        plt.ylabel( "number of candidates" )
        plt.yscale("log")
        plt.legend()
        plt.savefig(path.join(self.DIR_NAME,"response_curve_binary_among_classes_normed.pdf"), transparent = False)
        plt.clf()
        plt.hist( background_preds, list(np.linspace(0,1,100)), color = "red", alpha = 0.5, label = "background candidates" )
        plt.hist( signal_Bd_preds, list(np.linspace(0,1,100)), color = "green", alpha = 0.5, label = "signal Bd candidates" )
        plt.hist( signal_Bdbar_preds, list(np.linspace(0,1,100)), color = "blue", alpha = 0.5, label = "signal Bdbar candidates" )
        plt.xlabel( "signal probability" )
        plt.ylabel( "number of candidates" )
        plt.yscale("log")
        plt.legend()
        plt.savefig(path.join(self.DIR_NAME,"response_curve_binary_among_classes.pdf"), transparent = False)
        plt.clf()
        plt.hist( background_preds, list(np.linspace(0,1,100)), color = "red", alpha = 0.5, label = "background candidates", density = True )
        plt.hist( signal_Bd_preds_Bd, list(np.linspace(0,1,100)), color = "green", alpha = 0.5, label = "signal Bd candidates", density = True )
        plt.hist( signal_Bdbar_preds_Bdbar, list(np.linspace(0,1,100)), color = "blue", alpha = 0.5, label = "signal Bdbar candidates", density = True )
        plt.xlabel( "probability to be X for X" )
        plt.ylabel( "number of candidates" )
        plt.yscale("log")
        plt.legend()
        plt.savefig(path.join(self.DIR_NAME,"response_curve_normed.pdf"), transparent = False)
        plt.clf()
        plt.hist( 1 - background_preds, list(np.linspace(0,1,100)), color = "red", alpha = 0.5, label = "background candidates", density = True )
        plt.hist( signal_preds, list(np.linspace(0,1,100)), color = "green", alpha = 0.5, label = "signal Bd candidates", density = True )
        plt.xlabel( "signal probability" )
        plt.ylabel( "number of candidates" )
        plt.yscale("log")
        plt.legend()
        plt.savefig(path.join(self.DIR_NAME,"response_curve_binary_normed.pdf"), transparent = False)
        plt.clf()
        plt.hist( 1 - background_preds, list(np.linspace(0,1,100)), color = "red", alpha = 0.5, label = "background candidates", density = False )
        plt.hist( signal_preds, list(np.linspace(0,1,100)), color = "green", alpha = 0.5, label = "signal Bd candidates", density = False )
        plt.xlabel( "signal probability" )
        plt.ylabel( "number of candidates" )
        plt.yscale("log")
        plt.legend()
        plt.savefig(path.join(self.DIR_NAME,"response_curve_binary.pdf"), transparent = False)



    def plot_parametric_curve(self, x_metrics, y_metrics):
        if(isinstance(x_metrics, list) is not True):
            x_metrics = [x_metrics]
        if(isinstance(y_metrics, list) is not True):
            y_metrics = [y_metrics]
        x_metric_arrays = []
        y_metric_arrays = []
        legends = []
        counter = 0
        plotname = ""
        for x_metric in x_metrics:
            x_metric_array, y_metric_array = self.plot_metrics_over_proba_cuts([x_metrics[counter], y_metrics[counter]], return_arrays = True)
            x_metric_arrays.append(x_metric_array)
            y_metric_arrays.append(y_metric_array)
            legend = "x: "+x_metrics[counter] + ", y: " + y_metrics[counter]
            legends.append(legend)
            plotname += x_metrics[counter]+"_vs_"+y_metrics[counter] + "_"
            counter += 1
        plotname = path.join(self.DIR_NAME, plotname + "parametric_curves.pdf")
        self.plot_arrays(yarrays = y_metric_arrays, xarrays = x_metric_arrays, legends = legends, axes = ["x_value", "y_value"], name = plotname)



# Main...

def train(input_parameters):
    parameters = input_parameters.copy()

    print(utils.yellow("User setup:"))
    ml_parameters.check_gnn_parameters(parameters) # TODO
    print()

    print(utils.yellow("Input data..."))
    input_data = ml_data_preparation.prepare_data(parameters)

    utils.json_save(parameters, path.join(parameters["output_dir"], "model_details.json"), "Model details saved to:")
    print()

    samples, num_node_features, num_edge_features, num_goblin_features, num_nodes, num_edges = gnn_create_graphs.preparator(input_data, parameters, return_nums = True)

    # trainer = Trainer(dataset = samples, collate_function = gnn_create_graphs.collate_graphs, dir_name = parameters["output_dir"])
    parameters[ "num_node_features" ] = num_node_features
    parameters[ "num_edge_features"] = num_edge_features
    parameters[ "num_goblin_features"] = num_goblin_features
    parameters[ "num_nodes" ] = num_nodes
    parameters[ "num_edges" ] = num_edges

    trainer = Trainer(dataset = samples, collate_function = gnn_create_graphs.collate_graphs, dir_name = parameters["output_dir"], batch_size = parameters["batch_size"], n_epochs = parameters["epochs"], learning_rate = parameters["learning_rate"], parameters = parameters)
    training_metrics, validation_metrics = trainer.train()

    plotter = Plotter( validation_metrics = validation_metrics, training_metrics = training_metrics , dir_name = parameters["output_dir"])
    plotter.plot_metrics_over_epochs("loss")
    # plotter.plot_response_curve(trainer.validation_evaluator.preds, trainer.validation_evaluator.truths)
    # plotter.plot_metrics_over_proba_cuts(["signal_efficiency", "background_rejection"])


# Apply

# TODO: parameters
def apply_gnn(input_data, model_info, suffix, selection = "", selection_additional_after = "", n_best_chi2 = 0, full_output = False, batch_size = 1024):

    if n_best_chi2 > 0:
        print(utils.red("WIP!"))
        quit()

    items_to_check = [
        "type",
        "graph_dict",
        "graph_version",
        "classification_type",
        "classifier_name",
        "norm_base",
        "norm_base_proba",
        "features",
        "n_best_chi2",
        "n_best_prob",
        "n_best_prob_name",
        "n_best",
        "n_best_feature",
        "n_best_sort",
        "selection",
        "probability_cut",
        "labels_scheme",
        "additional_features_to_load",
        "selection_additional_before",
        "selection_additional_after",
        "sample_weight_type",
        "norm_base_size",
    ]

    if not isinstance(model_info, list):
        model_info = [ model_info ]

    if not isinstance(suffix, list):
        suffix = [ suffix ]

    model_dicts = [ utils.json_load(x) for x in model_info ]

    for model_dict, model_name in zip(model_dicts, model_info):
        for item in items_to_check:
            if item in model_dict:
                if model_dict[item] != model_dicts[0][item]:
                    print(utils.red(sys._getframe().f_code.co_name + ": model \"" + model_name + "\" is different (" + item + ")!"))
                    quit()

    dir_path           = path.dirname(model_info[0])
    norm_base_path     = path.join(dir_path, "norm_base.csv")
    normalization_path = path.join(dir_path, "normalization.csv")

    items_to_load = model_dicts[0]["features"] + model_dicts[0]["additional_features_to_load"] + utils.get_features_from_string(model_dicts[0]["selection_additional_after"]) + [ "info_truth_matching", "info_weight", "info_full_selection_no_B_mass", "info_full_selection", "info_sample_event_number", "info_sample", "info_event_number" ]

    print(utils.yellow("Input data..."))
    input_data = utils.df_load(input_data = input_data, items_to_load = items_to_load, selection = selection)

    # TODO: n_best_prob and other nn stuff?

    # if n_best_chi2 > 0:
    #     input_data = utils.get_best_chi2(input_data, n_best_chi2)

    input_data = utils.df_view(input_data, selection_additional_after)

    print()

    # TODO: make function...

    print(utils.yellow("Normalization..."))
    if path.isfile(normalization_path):
        norm_base = utils.df_load(normalization_path)
    elif path.isfile(norm_base_path):
        norm_base = utils.df_load(norm_base_path)

        norm_dict = { "_norm": [ "mean", "std" ] }

        for feature in model_dicts[0]["features"]:
            norm_dict[feature] = [ norm_base[feature].mean(), norm_base[feature].std() ]

        utils.df_save(pd.DataFrame(norm_dict), normalization_path, "Normalization saved to:")
    else:
        norm_base = utils.df_load(input_data = model_dicts[0]["norm_base"], selection = model_dicts[0]["selection"] + ( " & " if model_dicts[0]["selection_additional_before"] else "" ) + model_dicts[0]["selection_additional_before"], probabilities = model_dicts[0]["norm_base_proba"], probability_cut = model_dicts[0]["probability_cut"])

        if model_dicts[0]["n_best_chi2"] > 0:
            norm_base = utils.get_best_chi2(norm_base, model_dicts[0]["n_best_chi2"])
        elif model_dicts[0]["n_best"] > 0:
            norm_base = utils.get_best_chi2(norm_base, n = model_dicts[0]["n_best"], feature = model_dicts[0]["n_best_feature"], sort = model_dicts[0]["n_best_sort"])

        norm_base = utils.df_view(norm_base, model_dicts[0]["selection_additional_after"])
        utils.df_save(norm_base, norm_base_path, "Norm base saved to:")

        norm_dict = { "_norm": [ "mean", "std" ] }

        for feature in model_dicts[0]["features"]:
            norm_dict[feature] = [ norm_base[feature].mean(), norm_base[feature].std() ]

        utils.df_save(pd.DataFrame(norm_dict), normalization_path, "Normalization saved to:")

    # # This call is now inside the preparator() function:
    # # ml_data_preparation.normalize_df(input_data, norm_base, model_dicts[0]["features"])

    # # This was some test...
    # # input_data = input_data.loc[:,~input_data.columns.duplicated()]

    print()

    samples, num_node_features, num_edge_features, num_goblin_features, num_nodes, num_edges = gnn_create_graphs.preparator(input_data, model_dicts[0], return_nums = True)

    samples = DataLoader(samples, batch_size = batch_size, shuffle = False, collate_fn = gnn_create_graphs.collate_graphs)

    hyperparameters = {}
    hyperparameters[ "num_node_features"  ] = num_node_features
    hyperparameters[ "num_edge_features"  ] = num_edge_features
    hyperparameters[ "num_goblin_features"] = num_goblin_features
    hyperparameters[ "num_nodes" ] = num_nodes
    hyperparameters[ "num_edges" ] = num_edges
    hyperparameters[ "goblins_infect_broadcast" ] = "infect_broadcast" in model_dicts[0]["goblins_plan"]
    #hyperparameters[ "update_order" ] = parameters[ "graph_dict" ][ "update_order" ]

    df_return = []

    for model_name, sfx in zip(model_info, suffix):
        model_path  = path.join(path.dirname(model_name), "trained_model.pt")
        print(utils.yellow("Applying model " + model_path))

        # classifier = gnn_models.get_classifier(model_dicts[0]["classifier_name"])
        # classifier = gnn_models.get_classifier(classifier_name = model_dicts[0]["classifier_name"], num_node_features = num_node_features, num_edge_features = num_edge_features)
        classifier = gnn_models.get_classifier(model_dicts[0]["classifier_name"], hyperparameters )
    
        classifier.load_state_dict(torch.load(model_path, map_location = torch.device(ml_device.device)))

        evaluator = Evaluator(dataloader = samples, classifier = classifier, parameters = model_dicts[0]) # semi-HACK (parameters)

        df_pred_proba = evaluator.evaluate(suffix = sfx)[0]

        if len(input_data) != len(df_pred_proba):
            print(utils.red(sys._getframe().f_code.co_name + ": different lengths!"))
            print(input_data.shape)
            print(df_pred_proba.shape)
            quit()

        if full_output:
            df_new = input_data.join(df_pred_proba)

            if df_new.isnull().values.any() or len(df_new) != len(df_pred_proba):
                print(red(sys._getframe().f_code.co_name + ": join() error!"))
                quit()

            df_return += [ df_new ]
        else:
            df_return += [ df_pred_proba ]

    return df_return
