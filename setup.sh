#!/bin/bash
(
  [[ -n $BASH_VERSION ]] && (return 0 2>/dev/null)
) && sourced=1 || sourced=0

if [[ "${sourced}" == 0 ]]; then
    echo "Please do not `execute` this file, but `source` it."
    echo "See, e.g., https://superuser.com/questions/176783/."
    exit 1
fi

echo "Adding the current directory to the PYTHONPATH..."
dir="$( dirname $( readlink -f "${BASH_SOURCE[0]}" ) )"
! echo "$PYTHONPATH" | egrep "(^|:)${dir%/}/?(:|$)" &>/dev/null && export PYTHONPATH="${dir}:${PYTHONPATH}"

if [[ "$1" == "virtual" ]]; then
    echo "Creating a virtual environment for python..."
    virtual=1
    python -m venv .virtualenv
    source .virtualenv/bin/activate
else
    echo "Proceeding in your default environment for python..."
fi

echo "Installing the necessary python packages..."
python -m pip install pip --upgrade
python -m pip install -r requirements.txt