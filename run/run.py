import argparse

from src.utils import logger
from src.utils import configure

logger = logger.get_logger("ML4HEP")

argparser = argparse.ArgumentParser(description=__doc__)
argparser.add_argument(
    "--config", "-c", default="config_v1.yaml", help="Path to the config file."
)
argparser.add_argument("--log", "-l", default="INFO", help="Logging level.")
argparser.add_argument(
    "--actions", "-a", nargs="+", default=None, help="Actions to perform."
)

args = argparser.parse_args()
config = configure.load_config(args.config)
if args.actions is None:
    logger.info("No actions specified. Performing all actions.")
    actions = config["actions"]
else:
    actions = args.actions
if "process_ntuples" in actions:
    logger.info("Processing ntuples...")
    from src.data_preparation import executer 
    executer.process_ntuples(config["ntuples"], logger=logger)
    logger.info("Done processing ntuples!")

if "train" in actions:
    logger.info("Training...")
    from src.ml_core import executer
    executer.train(config["training"], logger=logger)
    logger.info("Done training!")

if "apply" in actions:
    logger.info("Applying...")
    from src.ml_core import executer
    executer.apply(config["application"], logger=logger)
    logger.info("Done applying!")

if "analyze" in actions:
    logger.info("Analyzing...")
    from src.analysis import executer
    executer.run(config["analysis"], logger=logger)
    logger.info("Done analyzing!")
